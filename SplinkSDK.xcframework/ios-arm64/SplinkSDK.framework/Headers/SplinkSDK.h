//
//  SplinkSDK.h
//  SplinkSDK
//
//  Created by Roberto Dias on 22/02/2022.
//

#import <Foundation/Foundation.h>

//! Project version number for SplinkSDK.
FOUNDATION_EXPORT double SplinkSDKVersionNumber;

//! Project version string for SplinkSDK.
FOUNDATION_EXPORT const unsigned char SplinkSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SplinkSDK/PublicHeader.h>


