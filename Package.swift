// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "UnityFramework",
    platforms: [.iOS(.v13)],
    products: [
        .library(name: "SplinkSDK", targets: ["SplinkSDKWrapper"]),
    ],
    dependencies: [
        
    ],
    targets: [
        .target(
          name: "SplinkSDKWrapper",
          dependencies: [
            .target(name: "SplinkSDK"),
            .target(name: "UnityFramework"),
          ]
        ),
        .binaryTarget(name: "SplinkSDK", path: "SplinkSDK.xcframework"),
        .binaryTarget(name: "UnityFramework", path: "UnityFramework.xcframework")
    ]
)
