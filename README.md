## Installation

### Swift Package Manager

The [Swift Package Manager](https://swift.org/package-manager/) is a tool for automating the distribution of Swift code and is integrated into the `swift` compiler. 

Once you have your Swift package set up, adding SplinkSDK as a dependency is as easy as adding it to the `dependencies` value of your `Package.swift`.

```swift
dependencies: [
    .package(url: "https://bitbucket.org/besplink/public-ar-sdk", .upToNextMajor(from: "1.3.8"))
]
```


## Using SplinkSDK

### Limitations
- SplinkSDK as a Library only supports full-screen rendering, and doesn’t support rendering on part of the screen.
- You can’t load more than one instance of the SplinkSDK runtime.
- SplinkSDK does not support `SceneDelegate`.
- SplinkSDK does not run on the simulator.
- SplinkSDK does not suport Bitcode.


## Removing `SceneDelegate` (If ussed)
- Remove `Application Scene Manifest` from `Info.plist`.
- Delete `SceneDelegate` file.
- Add this line `var window: UIWindow?` to your `AppDelegate`.
- Remove the `UISceneSession Lifecycle` methods from your `AppDelegate`.

### Integration
- Disable Bitcode under Build Settings.
- On your `AppDelegate` initialize the SDK
```swift
Splink.initialise(key: "YOUR KEY", window: window)
```
- The just open the AR experience with the opne method
```swift
Splink.shared.open(action: .player(ean: "PLAYER_EAN"))
```
